#include "ros/ros.h"
#include "my_service/ConcatStr.h"

bool concatinate(my_service::ConcatStr::Request &req, my_service::ConcatStr::Response &res)
{
    res.concat = req.first + req.second;
    ROS_INFO("[%d] + [%d]\n", req.first, req.second);
    ROS_INFO("[%d]\n", res.concat);
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "add_two_str_server");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("add_two_str", concatinate);
    ROS_INFO("Ready");

    ros::spin();
    return 0;
}