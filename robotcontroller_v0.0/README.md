# Gazebo ROS Demos

* Author: Ildar Shamsiev <movemorph@gmail.com>
* License: GNU General Public License, version 3 (GPL-3.0)

Example robots and code for interfacing Gazebo with ROS


## Quick Start

Rviz:

    roslaunch rrbot_description rrbot_rviz.launch

Gazebo:

    roslaunch rrbot_gazebo rrbot_world.launch

ROS Control:

    roslaunch rrbot_control rrbot_control.launch

Example of Moving Joints:

    rostopic pub /rrbot/joint2_position_controller/command std_msgs/Float64 "data: -0.9"

## Develop and Contribute

Я рад приветствовать любые вклады в это репо и рекомендуем вам раскошелиться на проект, а затем отправлять запросы на извлечение в это родительское репо. Спасибо за вашу помощь!
